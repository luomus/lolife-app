# LoLIFE-App

LoLIFE-App on mobiilisovellus kuntien ja ELY-keskusten ympäristöviranomaisille liito-oravien kartoitukseen. Sovelluksessa merkitään kartalle havaintoja liito-oravista ja niihin liittyvistä havainnoista, esim. liito-oravien pesistä. Sovellusta
voidaan käyttää maastossa, jolloin käyttäjä voi kirjata sijaintipaikallaan tekemiä havaintoja. Sovellus käyttää paikkatietoja käyttäjän kulkeman reitin merkitsemiseen. Sovelluksessa käyttäjä muodostaa retkiä, eli ns. havaintotapahtumia, jotka 
koostuvat käyttäjän retken aikana merkitsemistä havainnoista ja kuljetusta reitistä. Sovelluksella lähetetään havaintotapahtumia Laji.fi tietokantaan.

LoLIFE-App is a mobile application for the Finnish government officials to map observations of flying squirrels. Observations of flying squirrels and related observations (flying squirrel nests, for example) are marked to the map in the
application. The app can be used in the terrain, so that the users can mark the observations which they find near their location. The app uses location data to track the path which the user has traveled. The user forms "observation events" in
the application, which consist of the observations and the traveled path during this journey. Observation events are sent to Laji.fi database with the application.

### Dokumentaatio - Documentation

* [Kehitysympäristön asennusohjeet / Development environment installation guide](docs/installation_instructions.md)
