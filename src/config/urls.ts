//mapurl
export const mapUrl               = 'https://proxy.laji.fi/mml_wmts/maasto/wmts/1.0.0/maastokartta/default/WGS84_Pseudo-Mercator/{z}/{y}/{x}.png'

//api root url
const apiRoot                     = 'https://api.laji.fi/v0'

//graphql url
export const graphqlUrl           = `${apiRoot}/graphql`

//documents url
export const postDocumentUrl      = `${apiRoot}/documents`

//image urls
export const postImageUrl         = `${apiRoot}/images`

//taxon autocomplete urls
export const autocompleteTaxonUrl = `${apiRoot}/autocomplete/taxon`

//locality url
export const localityUrl          = `${apiRoot}/coordinates/location`

//login urls
export const getLoginUrl          = `${apiRoot}/login`
export const pollLoginUrl         = `${apiRoot}/login/check`
export const getUserUrl           = `${apiRoot}/person/`
export const personTokenUrl       = `${apiRoot}/person-token/`

//observation zone urls
export const getZonesUrl          = `${apiRoot}/named-places`

//privacy policy
export const privacyPolicyEn      = 'https://laji.fi/about/848'
export const privacyPolicyFi      = 'https://laji.fi/about/713'

//about lolife
export const aboutLoLifeEn        = 'https://laji.fi/en/project/MHL.45/about'
export const aboutLoLifeFi        = 'https://laji.fi/project/MHL.45/about'
export const aboutLoLifeSv        = 'https://laji.fi/sv/project/MHL.45/about'

//laji.fi observations page
export const lajiFI               = 'https://laji.fi/observation/list?collectionId=HR.2951'
export const lajiSV               = 'https://laji.fi/sv/observation/list?collectionId=HR.2951'
export const lajiEN               = 'https://laji.fi/en/observation/list?collectionId=HR.2951'

//google geocoding api url
export const googleGeocodingAPIURL = 'https://maps.googleapis.com/maps/api/geocode/json'