import {
  SchemaType,
  schemaActionTypes,
  SET_SCHEMA
} from './types'

const initSchemaState: SchemaType = {
  error: null,
  loading: false,
  fi: null,
  en: null,
  sv: null,
}

const schemaReducer = (state = initSchemaState, action : schemaActionTypes) => {
  switch (action.type) {
    case SET_SCHEMA:
      return {
        ...action.payload,
      }
    default:
      return state
  }
}

export {
  schemaReducer
}