import { ThunkAction } from 'redux-thunk'
import uuid from 'react-native-uuid'
import { clone, set } from 'lodash'
import { LocationObject } from 'expo-location'
import {
  toggleCentered,
  setFirstZoom,
  clearRegion,
  setCurrentObservationZone
} from '../map/actions'
import {
  setMessageState
} from '../message/actions'
import {
  removeDuplicatesFromPath,
  clearObservationLocation,
  setObservationEventInterrupted,
  replaceObservationEventById,
  replaceObservationEvents,
  toggleObserving,
  clearObservationId
} from '../observation/actions'
import {
  clearLocation,
  updateLocation,
  clearPath,
  setPath,
  setFirstLocation
} from '../position/actions'
import { mapActionTypes, ZoneType } from '../map/types'
import { messageActionTypes } from '../message/types'
import { observationActionTypes } from '../observation/types'
import { PathType } from '../position/types'
import i18n from '../../languages/i18n'
import storageService from '../../services/storageService'
import userService from '../../services/userService'
import { parseSchemaToNewObject } from '../../helpers/parsers/SchemaObjectParser'
import { setDateForDocument } from '../../helpers/dateHelper'
import { log } from '../../helpers/logger'
import { stopLocationAsync, watchLocationAsync } from '../../helpers/geolocationHelper'
import { locationActionTypes } from '../position/types'
import { createUnitBoundingBox } from '../../helpers/geometryHelper'
import { lineStringConstructor } from '../../helpers/geoJSONHelper'
import { sourceId } from '../../config/keys'

export interface ResetActionType {
  type: string,
}

export const resetReducer = (): ResetActionType => ({
  type: 'RESET_STORE'
})

export const beginObservationEvent = (onPressMap: () => void, zoneUsed: boolean, title: string, body: string): ThunkAction<Promise<any>, any, void,
  mapActionTypes | observationActionTypes | locationActionTypes | messageActionTypes> => {
  return async (dispatch, getState) => {

    const { centered, credentials, observationEvent, observationZone, schema } = getState()
    const userId = credentials?.user?.id

    const region: ZoneType | undefined = observationZone.zones.find((region: Record<string, any>) => {
      return region.id === observationZone.currentZoneId
    })

    if (!userId || (zoneUsed && !region)) {
      return
    }

    //check that person token isn't expired
    try {
      await userService.checkTokenValidity(credentials.token)
    } catch (error) {
      log.error({
        location: '/stores/shared/actions.tsx beginObservationEvent()',
        error: error
      })
      return Promise.reject({
        severity: 'low',
        message: i18n.t('user token has expired')
      })
    }

    if (!zoneUsed) {
      dispatch(setCurrentObservationZone('empty'))
    }

    const lang = i18n.language

    let observationEventDefaults = {}
    set(observationEventDefaults, 'editors', [userId])
    set(observationEventDefaults, 'sourceID', sourceId)
    set(observationEventDefaults, ['gatheringEvent', 'leg'], [userId])
    set(observationEventDefaults, ['gatheringEvent', 'dateBegin'], setDateForDocument())

    let parsedObservationEvent = parseSchemaToNewObject(observationEventDefaults, ['gatherings_0_units'], schema.schemas[lang].schema)

    const setGeometry = () => {
      set(parsedObservationEvent, ['gatherings', '0', 'geometry'], region?.geometry)
      set(parsedObservationEvent, ['gatherings', '0', 'locality'], region?.name)
      set(parsedObservationEvent, ['namedPlaceID'], region?.id)
    }

    if (zoneUsed) {
      setGeometry()
    }

    const observationEventObject = {
      id: 'observationEvent_' + uuid.v4(),
      formID: 'MHL.45',
      ...parsedObservationEvent
    }

    const newEvents = observationEvent.events.concat(observationEventObject)

    try {
      await storageService.save('observationEvents', newEvents)
    } catch (error) {
      log.error({
        location: '/stores/observation/actions.tsx newObservationEvent()',
        error: error
      })
      return Promise.reject({
        severity: 'low',
        message: i18n.t('could not save new event to long term memory, discarding modifications')
      })
    }

    dispatch(replaceObservationEvents(newEvents))

    //attempt to start geolocation systems
    try {
      await watchLocationAsync(
        (location: LocationObject) => dispatch(updateLocation(location)),
        title,
        body
      )
    } catch (error) {
      log.error({
        location: '/actionCreators/observationEventCreators.tsx beginObservationEvent()',
        error: error
      })
      dispatch(setMessageState({
        type: 'err',
        messageContent: error.message
      }))
      return Promise.reject()
    }

    //reset map centering and zoom level, redirect to map
    !centered ? dispatch(toggleCentered()) : null
    dispatch(clearRegion())
    dispatch(toggleObserving())
    dispatch(setFirstZoom('not'))
    onPressMap()

    return Promise.resolve()
  }
}

export const continueObservationEvent = (onPressMap: () => void, title: string, body: string): ThunkAction<Promise<any>, any, void,
  locationActionTypes | mapActionTypes | messageActionTypes | observationActionTypes> => {
  return async (dispatch, getState) => {
    const { centered, credentials, observationEventInterrupted, observationEvent } = getState()

    if (!observationEventInterrupted) {
      onPressMap()
      return Promise.resolve()
    }

    //check that person token isn't expired
    try {
      await userService.checkTokenValidity(credentials.token)
    } catch (error) {
      log.error({
        location: '/stores/shared/actions.tsx beginObservationEvent()',
        error: error
      })
      return Promise.reject({
        severity: 'low',
        message: i18n.t('user token has expired')
      })
    }

    //atempt to start geolocation systems
    try {
      await watchLocationAsync(
        (location: LocationObject) => dispatch(updateLocation(location)),
        title,
        body
      )
    } catch (error) {
      log.error({
        location: '/actionCreators/observationEventCreators continueObservationEvent()',
        error: error
      })
      dispatch(setMessageState({
        type: 'err',
        messageContent: error.message
      }))
      return Promise.reject()
    }

    dispatch(setObservationEventInterrupted(false))
    //reset map centering and zoom level
    !centered ? dispatch(toggleCentered()) : null
    dispatch(clearRegion())

    //set old path if exists
    const path: PathType = observationEvent.events?.[observationEvent.events.length - 1].gatherings[1]?.geometry.coordinates
    if (path) {
      dispatch(setPath(path))
    }

    dispatch(setFirstZoom('not'))
    onPressMap()
    return Promise.resolve()
  }
}

export const finishObservationEvent = (): ThunkAction<Promise<any>, any, void,
  locationActionTypes | mapActionTypes | messageActionTypes | observationActionTypes> => {
  return async (dispatch, getState) => {
    const { firstLocation, observationEvent, path, observationEventInterrupted } = getState()

    dispatch(setObservationEventInterrupted(false))

    let event = clone(observationEvent.events?.[observationEvent.events.length - 1])

    if (event) {
      let lineStringPath = lineStringConstructor(path)

      if (lineStringPath) {
        //remove duplicates from path
        lineStringPath.coordinates = removeDuplicatesFromPath(lineStringPath.coordinates)

        if (lineStringPath.coordinates.length >= 2) {
          event.gatherings[1].geometry = lineStringPath
        }
      }

      if (!event.namedPlaceID || event.namedPlaceID === '') {
        const setBoundingBoxGeometry = () => {
          let geometry

          if (event.gatherings[0].units.length >= 1) {
            geometry = createUnitBoundingBox(event)
          } else {
            geometry = {
              coordinates: [
                firstLocation[1],
                firstLocation[0]
              ],
              type: 'Point'
            }
          }

          if (geometry) {
            event.gatherings[0].geometry = geometry
          }
        }

        if (lineStringPath) {
          if (lineStringPath.coordinates.length >= 2) {
            event.gatherings[0].geometry = lineStringPath
          } else {
            setBoundingBoxGeometry()
          }
        } else {
          setBoundingBoxGeometry()
        }
      }

      dispatch(clearPath())
      dispatch(clearLocation())

      //replace events with modified list
      try {
        dispatch(replaceObservationEventById(event, event.id))
      } catch (error) {
        dispatch(setMessageState({
          type: 'err',
          messageContent: error.message
        }))
        return Promise.reject()
      }
    }

    dispatch(toggleObserving())
    dispatch(clearObservationLocation())
    dispatch(clearObservationId())
    dispatch(setFirstZoom('not'))
    dispatch(setFirstLocation([60.192059, 24.945831]))
    await stopLocationAsync(observationEventInterrupted)

    return Promise.resolve()
  }
}