import {
  userActionTypes,
  SET_CREDENTIALS,
  CLEAR_CREDENTIALS,
  CredentialsType,
} from './types'
import { ThunkAction } from 'redux-thunk'
import userService, { pollUserLogin } from '../../services/userService'
import storageService from '../../services/storageService'
import i18n from '../../languages/i18n'
import { log } from '../../helpers/logger'

export const loginUser = (tmpToken: string, setCanceler: any): ThunkAction<Promise<any>, any, void, userActionTypes> => {
  return async dispatch => {
    let credentials: CredentialsType | null = null
    try {
      //start polling for credentials from server, error thrown when timeout of 180 seconds reached,
      //else dispatch to store
      credentials = await pollUserLogin(tmpToken, setCanceler)
      dispatch(setCredentials(credentials))

    //if timeout or other error inform user of error, credentials stay null
    } catch (netError) {
      if (netError.canceled) {
        return Promise.reject({ canceled: true })
      }
      if (!netError.timeout) {
        log.error({
          location: '/stores/user/actions.tsx loginUser()',
          error: netError.response.data.error
        })
        return Promise.reject({
          severity: 'fatal',
          message: i18n.t('failed to load credentials from server')
        })
      }

      return Promise.reject({
        severity: 'fatal',
        message: i18n.t('login timed out')
      })
    }

    //try to save credentials to asyncstorage to remember logged in user after app shutdown
    try {
      storageService.save('credentials', credentials)
      return Promise.resolve()

    //if asyncstorage fails set error as such, allows user to continue anyway
    } catch (locError) {
      log.error({
        location: '/stores/user/actions.tsx loginUser()',
        error: locError
      })
      return Promise.reject({
        severity: 'low',
        message: i18n.t('failed to save credentials locally')
      })
    }
  }
}

export const logoutUser = (): ThunkAction<Promise<any>, any, void, userActionTypes> => {
  return async (dispatch, getState) => {
    const { credentials } = getState()

    try {
      storageService.remove('credentials')
      dispatch(clearCredentials())

    } catch (error) {
      log.error({
        location: '/stores/user/actions.tsx logoutUser()',
        error: error
      })
      return Promise.reject({
        severity: 'low',
        message: i18n.t('failed to remove credentials from local storage')
      })
    }

    try {
      await userService.logout(credentials)

    } catch (error) {
      log.error({
        location: '/stores/user/actions.tsx logoutUser()',
        error: error
      })
      return Promise.reject({
        severity: 'low',
        message: i18n.t('failed to logout from laji.fi server')
      })
    }

    return Promise.resolve
  }
}

export const initLocalCredentials = (): ThunkAction<Promise<any>, any, void, userActionTypes> => {
  return async dispatch => {
    try {
      const credentials = await storageService.fetch('credentials')

      if (!credentials) {
        return Promise.reject()
      }

      dispatch(setCredentials(credentials))
      return Promise.resolve()
    } catch (error) {
      log.error({
        location: '/stores/user/actions.tsx initLocalCredentials()',
        error: error
      })
      return Promise.reject({
        severity: 'low',
        message: i18n.t('failed to load credentials from local')
      })
    }
  }
}

export const setCredentials = (credentials: CredentialsType): userActionTypes => ({
  type: SET_CREDENTIALS,
  payload: credentials,
})

export const clearCredentials = (): userActionTypes => ({
  type: CLEAR_CREDENTIALS
})