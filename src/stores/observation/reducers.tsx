import {
  ObservationEventType,
  observationActionTypes,
  CLEAR_OBSERVATION,
  SET_OBSERVATION,
  SET_OBSERVATION_EVENT_INTERRUPTED,
  CLEAR_OBSERVATION_EVENTS,
  NEW_OBSERVATION_EVENT,
  REPLACE_OBSERVATION_EVENTS,
  CLEAR_OBSERVATION_ID,
  SET_OBSERVATION_ID,
  TOGGLE_OBSERVING
} from './types'

const initObsEventState: ObservationEventType = {
  events: []
}

const observationReducer = (state = null, action : observationActionTypes) => {
  switch (action.type) {
    case CLEAR_OBSERVATION:
      return null
    case SET_OBSERVATION:
      return action.payload
    default:
      return state
  }
}

const observationEventInterruptedReducer = (state: boolean = false, action : observationActionTypes) => {
  switch (action.type) {
    case SET_OBSERVATION_EVENT_INTERRUPTED:
      return action.payload
    default:
      return state
  }
}

const observationEventsReducer = (state: ObservationEventType = initObsEventState, action : observationActionTypes) => {
  let newState
  switch (action.type) {
    case CLEAR_OBSERVATION_EVENTS:
      newState = {
        ...state,
        events: []
      }
      return newState
    case NEW_OBSERVATION_EVENT:
      newState = {
        ...state,
        events: state.events.concat(action.payload)
      }
      return newState
    case REPLACE_OBSERVATION_EVENTS:
      newState = {
        ...state,
        events: action.payload
      }
      return newState
    default:
      return state
  }
}

const observationIdReducer = (state = null, action : observationActionTypes) => {
  switch (action.type) {
    case CLEAR_OBSERVATION_ID:
      return null
    case SET_OBSERVATION_ID:
      return action.payload
    default:
      return state
  }
}

const observingReducer = (state = false, action : observationActionTypes) => {
  switch (action.type) {
    case TOGGLE_OBSERVING:
      return !state
    default:
      return state
  }
}

export {
  observationReducer,
  observationEventInterruptedReducer,
  observationEventsReducer,
  observationIdReducer,
  observingReducer
}