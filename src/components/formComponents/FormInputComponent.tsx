import React, { useState, useEffect } from 'react'
import { Text, TextInput, View } from 'react-native'
import { useFormContext } from 'react-hook-form'
import Os from '../../styles/OtherStyles'
import Cs from '../../styles/ContainerStyles'

interface Props {
  title: string,
  objectTitle: string,
  parentObjectTitle: string,
  keyboardType:
    'default' |
    'email-address' |
    'numeric' |
    'phone-pad' |
    'visible-password' |
    'ascii-capable' |
    'numbers-and-punctuation' |
    'url' |
    'number-pad' |
    'name-phone-pad' |
    'decimal-pad' |
    'twitter' |
    'web-search' |
    undefined,
  defaultValue: string | number | undefined,
  isArrayItem: boolean,
  parentCallback: Function | undefined,
  editable: boolean,
}

const FormInputComponent = (props: Props) => {

  const [currentValue, setCurrentValue] = useState<string>(props.defaultValue ? props.defaultValue.toString() : '')
  const { register, setValue } = useFormContext()

  useEffect(() => {
    if (!props.isArrayItem)
      initField()
  }, [])

  const parseInput = (input: string) => {
    return props.keyboardType === 'numeric' ? input !== '' ? parseInt(input) : undefined : input
  }

  const initField = () => {
    register(props.objectTitle)
    setValue(props.objectTitle, parseInput(currentValue))
  }

  return (
    <View style={props.isArrayItem ? Cs.formArrayInputContainer : Cs.formInputContainer}>
      {!props.isArrayItem
        ? <Text>{props.title}</Text>
        : null
      }
      <TextInput
        style={Os.textInput}
        keyboardType={props.keyboardType}
        editable={props.editable}
        onChangeText={text => {
          props.isArrayItem && props.parentCallback
            ? props.parentCallback({ objectTitle: props.objectTitle, value: parseInput(text) })
            : setValue(props.objectTitle, parseInput(text))
          setCurrentValue(text)
        }}
        defaultValue={currentValue}
      />
    </View>
  )
}

export default FormInputComponent