import React from 'react'
import { Dimensions, Linking, ScrollView, Text, View } from 'react-native'
import { privacyPolicyEn, privacyPolicyFi } from '../../config/urls'
import { useTranslation } from 'react-i18next'
import Cs from '../../styles/ContainerStyles'
import Ts from '../../styles/TextStyles'
import { Image } from 'react-native-elements'

export const InfoPageComponent = () => {
  const { t, i18n } = useTranslation()

  let privacyPolicy = ''

  if (i18n.language === 'fi') {
    privacyPolicy = privacyPolicyFi
  } else if (i18n.language === 'sv') {
    privacyPolicy = privacyPolicyFi
  } else (
    privacyPolicy = privacyPolicyEn
  )

  return (
    <ScrollView style={Cs.infoContainer}>
      <View style={Cs.infoImageContainer}>
        <Image
          style={{
            width: Dimensions.get('window').width * 0.80,
            height: Dimensions.get('window').width * 0.80,
          }}
          source={require('../../../assets/splashscreen_image.png')}
          resizeMode='contain'
        />
      </View>
      <View style={Cs.infoTextContainer}>
        <Text style={Ts.infoText}>
          {`${t('see privacy policy')} `}
          <Text style={{ color: 'blue' }} onPress={() => Linking.openURL(privacyPolicy)}>
            {`${t('privacy policy')}\n\n`}
          </Text>
          <Text>
            {t('infotext.lolife.1') + '\n\n' + t('infotext.lolife.2')}
          </Text>
        </Text>
      </View>
    </ScrollView>
  )
}