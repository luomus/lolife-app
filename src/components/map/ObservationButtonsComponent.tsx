import React from 'react'
import { StyleProp, View, ViewStyle, Text } from 'react-native'
import ButtonComponent from '../general/ButtonComponent'
import Cs from '../../styles/ContainerStyles'
import Bs from '../../styles/ButtonStyles'
import Ts from '../../styles/TextStyles'
import Colors from '../../styles/Colors'
import { useSelector } from 'react-redux'
import { useTranslation } from 'react-i18next'
import { rootState } from '../../stores'
import i18n from '../../languages/i18n'
import { listOfHaversineNeighbors } from '../../helpers/distanceHelper'

interface BasicObject {
  [key: string]: any
}

type Props = {
  confirmationButton: (rules?: Record<string, any>, defaults?: Record<string, any>, type?: string, sourcePage?: string) => void,
  cancelButton: () => void,
  mode: string,
  openModal: (units: Array<Record<string, any>>, eventId: string) => void,
  shiftToEditPage: (eventId: string, unitId: string) => void
}

const ObservationButtonsComponent = (props: Props) => {

  const observation = useSelector((state: rootState) => state.observation)
  const observationEvent = useSelector((state: rootState) => state.observationEvent)
  const region = useSelector((state: rootState) => state.region)
  const schema = useSelector((state: rootState) => state.schema)

  const { t } = useTranslation()

  const createButton = (title: string, buttonStyle: StyleProp<ViewStyle>, onPress: () => void, styleType: string,
    iconName: string | undefined, iconType: string | undefined, width: number) => {
    return (
      <View key={title} style={Cs.observationTypeButton}>
        <ButtonComponent onPressFunction={() => onPress()} title={title}
          height={40} width={width} buttonStyle={buttonStyle}
          gradientColorStart={styleType === 'primary' ? Colors.primaryButton1 : Colors.neutralButton}
          gradientColorEnd={styleType === 'primary' ? Colors.primaryButton2 : Colors.neutralButton}
          shadowColor={styleType === 'primary' ? Colors.primaryShadow : Colors.neutralShadow}
          textStyle={Ts.buttonText} iconName={iconName} iconType={iconType} iconSize={22}
          contentColor={styleType === 'primary' ? Colors.whiteText : Colors.darkText}
        />
      </View>
    )
  }

  const createLeftSideButtonsList = () => {
    const units: BasicObject[] = observationEvent.events?.[observationEvent.events.length - 1]
      .gatherings[0].units
    const haversineNeighbors: Array<Record<string, any>> = listOfHaversineNeighbors(units, region, observation)
    const eventId: string = observationEvent.events?.[observationEvent.events.length - 1].id

    if (haversineNeighbors.length >= 4) {
      return (
        (
          <View style={Cs.observationTypeButtonsColumnLeft}>
            <Text style={Ts.mapButtonsLeftTitle}>{t('edit observations')}:</Text>
            {createButton(
              haversineNeighbors.length + ' ' + t('observation count'),
              Bs.observationNeighborsButton,
              () => props.openModal(haversineNeighbors, eventId),
              'neutral', 'edit', 'material-icons', 180
            )}
          </View>
        )
      )
    } else if (haversineNeighbors.length >= 1) {
      return (
        <View style={Cs.observationTypeButtonsColumnLeft}>
          <Text style={Ts.mapButtonsLeftTitle}>{t('edit observations')}:</Text>
          {haversineNeighbors?.map((neighbor: Record<string, any>) =>
            createButton(
              t(neighbor.type),
              Bs.observationNeighborsButton,
              () => props.shiftToEditPage(eventId, neighbor.id),
              'primary', undefined, undefined, 180
            )
          )}
        </View>
      )
    }
  }

  const createRightSideButtonsList = () => {
    let lang = i18n.language
    const unitGroups = schema.schemas[lang]?.uiSchemaParams?.unitGroups
    unitGroups[0].type = 'flying squirrel'
    unitGroups[1].type = 'traces'
    unitGroups[2].type = 'nest'
    unitGroups[3].type = 'feces'

    return unitGroups?.map((observation: Record<string, any>) =>
      createButton(
        observation.button.label,
        Bs.observationButton,
        () => props.confirmationButton(observation.rules, observation.button.default, observation.type),
        'primary', undefined, undefined, 180
      )
    )
  }

  const observationButtons = () => {
    if (props.mode === 'newObservation') {
      return (
        <View style={Cs.observationTypeColumnsContainer}>
          {createLeftSideButtonsList() ? createLeftSideButtonsList() : <View></View>}
          <View style={Cs.observationTypeButtonsColumnRight}>
            {createRightSideButtonsList()}
            {createButton(
              t('cancel'),
              Bs.endButton,
              () => props.cancelButton(),
              'neutral', undefined, undefined, 180
            )}
          </View>
        </View>
      )
    }
    if (props.mode === 'changeLocation') {
      return (
        <View style={Cs.observationTypeButtonsColumnRight}>
          {createButton(
            t('save'),
            Bs.observationButton,
            () => props.confirmationButton(),
            'primary', undefined, undefined, 150
          )}
          {createButton(
            t('cancel'),
            Bs.endButton,
            () => props.cancelButton(),
            'neutral', undefined, undefined, 150
          )}
        </View>
      )
    }
  }

  return (
    <View style={Cs.observationTypeButtonsContainer}>
      {observationButtons()}
    </View>
  )
}

export default ObservationButtonsComponent