import React from 'react'
import { Linking, View, FlatList, Text } from 'react-native'
import Modal from 'react-native-modal'
import { useTranslation } from 'react-i18next'
import Cs from '../../styles/ContainerStyles'
import { aboutLoLifeEn, aboutLoLifeFi, aboutLoLifeSv } from '../../config/urls'

type Props = {
  isVisible: boolean,
  onClose: () => void
}

const InstructionModalComponent = (props: Props) => {
  const { t, i18n } = useTranslation()

  let aboutLoLife = ''

  if (i18n.language === 'fi') {
    aboutLoLife = aboutLoLifeFi
  } else if (i18n.language === 'sv') {
    aboutLoLife = aboutLoLifeSv
  } else {
    aboutLoLife = aboutLoLifeEn
  }

  //note: the following view consists of two flat lists as the link had to be included to the first
  //item somehow

  return (
    <Modal isVisible={props.isVisible} onBackButtonPress={props.onClose} onBackdropPress={props.onClose}>
      <View style={Cs.observationAddModal}>
        <FlatList
          data={[...Array(1).keys()].map(val => { return { key: `${val + 1}` } })}
          renderItem={({ item }) =>
            <View style={Cs.instructionContainer}>
              <Text>{item.key + '. '}</Text>
              <Text>
                <Text>
                  {t('instructions.lolife.1')}
                </Text>
                <Text style={{ color: 'blue' }} onPress={() => Linking.openURL(aboutLoLife)}>
                  {' ' + `${t('instructions.mobilevihko.link')}`}
                </Text>
              </Text>
            </View>
          }
        />
        <FlatList
          data={[...Array(6).keys()].map(val => { return { key: `${val + 2}` } })}
          renderItem={({ item }) =>
            <View style={Cs.instructionContainer}>
              <Text>{item.key + '. '}</Text><Text>{t('instructions.lolife.' + item.key)}</Text>
            </View>
          }
        />
      </View>
    </Modal>
  )
}

export default InstructionModalComponent