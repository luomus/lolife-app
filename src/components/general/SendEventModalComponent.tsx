import React from 'react'
import { View, Text } from 'react-native'
import Modal from 'react-native-modal'
import { useTranslation } from 'react-i18next'
import ButtonComponent from '../general/ButtonComponent'
import Bs from '../../styles/ButtonStyles'
import Cs from '../../styles/ContainerStyles'
import Ts from '../../styles/TextStyles'
import Colors from '../../styles/Colors'

type Props = {
  modalVisibility: boolean,
  onCancel: React.Dispatch<React.SetStateAction<boolean>> | (() => void),
  sendObservationEvent: () => Promise<void>
}

const SendEventModalComponent = (props: Props) => {

  const { t } = useTranslation()

  return (
    <Modal isVisible={props.modalVisibility} backdropOpacity={10} onBackButtonPress={() => { props.onCancel(false) }}>
      <View style={Cs.sendEventModalContainer}>
        <Text style={Cs.containerWithJustPadding}>
          {t('send observation event to server?')}
        </Text>
        <View style={Cs.padding5Container}>
          <ButtonComponent onPressFunction={() => { props.sendObservationEvent() }} title={t('send public')}
            height={40} width={250} buttonStyle={Bs.sendEventModalPositiveButton}
            gradientColorStart={Colors.successButton1} gradientColorEnd={Colors.successButton2} shadowColor={Colors.successShadow}
            textStyle={Ts.buttonText} iconName={'publish'} iconType={'material-community'} iconSize={22} contentColor={Colors.whiteText}
          />
        </View>
        <View style={Cs.padding5Container}>
          <ButtonComponent onPressFunction={() => { props.onCancel(false) }} title={t('do not submit')}
            height={40} width={250} buttonStyle={Bs.sendEventModalNegativeButton}
            gradientColorStart={Colors.neutralButton} gradientColorEnd={Colors.neutralButton} shadowColor={Colors.neutralShadow}
            textStyle={Ts.buttonText} iconName={'close'} iconType={'material-community'} iconSize={22} contentColor={Colors.darkText}
          />
        </View>
      </View>
    </Modal>
  )
}

export default SendEventModalComponent