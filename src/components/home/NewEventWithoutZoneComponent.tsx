import React from 'react'
import { View, Text } from 'react-native'
import ButtonComponent from '../general/ButtonComponent'
import Bs from '../../styles/ButtonStyles'
import Cs from '../../styles/ContainerStyles'
import Ts from '../../styles/TextStyles'
import Colors from '../../styles/Colors'
import { useTranslation } from 'react-i18next'

type Props = {
  onBeginObservationEvent: (zoneUsed: boolean) => void
}

const NewEventWithoutZoneComponent = (props: Props) => {

  const { t } = useTranslation()

  return (
    <View style={Cs.observationEventContainer}>
      <Text style={Ts.observationEventTitle}>
        {t('new observation event without zone')}
      </Text>
      <View style={Cs.buttonContainer}>
        <ButtonComponent onPressFunction={() => props.onBeginObservationEvent(false)} title={t('beginObservation')}
          height={40} width={300} buttonStyle={Bs.beginButton}
          gradientColorStart={Colors.primaryButton1} gradientColorEnd={Colors.primaryButton2} shadowColor={Colors.primaryShadow}
          textStyle={Ts.buttonText} iconName={'play-arrow'} iconType={'material-icons'} iconSize={22} contentColor={Colors.whiteText}
        />
      </View>
    </View>
  )
}

export default NewEventWithoutZoneComponent