import React, { useEffect, useState } from 'react'
import { View, Text } from 'react-native'
import { useSelector } from 'react-redux'
import { rootState } from '../../stores'
import ButtonComponent from '../general/ButtonComponent'
import Bs from '../../styles/ButtonStyles'
import Cs from '../../styles/ContainerStyles'
import Ts from '../../styles/TextStyles'
import Colors from '../../styles/Colors'
import { useTranslation } from 'react-i18next'

type Props = {
  onContinueObservationEvent: () => Promise<void>,
  stopObserving: () => void
}

const UnfinishedEventViewComponent = (props: Props) => {

  const [zoneName, setZoneName] = useState<string>('')

  const observationEvent = useSelector((state: rootState) => state.observationEvent)
  const observationEventInterrupted = useSelector((state: rootState) => state.observationEventInterrupted)
  const observationZone = useSelector((state: rootState) => state.observationZone)

  const { t } = useTranslation()

  useEffect(() => {
    const name = getCurrentZoneName()
    setZoneName(name)
  }, [observationZone])

  const getCurrentZoneName = (): string => {
    if (observationEvent.events[observationEvent.events.length - 1]?.gatherings[0].locality) {

      const zone = observationZone.zones.find((zone: Record<string, any>) => {
        return zone.id === observationZone.currentZoneId
      })

      return zone ? zone.name : ''
    }

    return t('no zone')
  }

  return (
    <View>
      <Text style={Ts.observationEventTitle}>
        {observationEventInterrupted ?
          t('interrupted observation event') :
          t('event')
        }
      </Text>
      <Text style={Ts.zoneText}>
        {t('observation zone')}: {zoneName}
      </Text>
      <View style={Cs.buttonContainer}>
        <View style={Cs.padding5Container}>
          <ButtonComponent onPressFunction={() => props.onContinueObservationEvent()} title={t('continue')}
            height={40} width={150} buttonStyle={Bs.continueButton}
            gradientColorStart={Colors.primaryButton1} gradientColorEnd={Colors.primaryButton2} shadowColor={Colors.primaryShadow}
            textStyle={Ts.buttonText} iconName={'map-outline'} iconType={'material-community'} iconSize={22} contentColor={Colors.whiteText}
          />
        </View>
        <View style={Cs.padding5Container}>
          <ButtonComponent onPressFunction={() => props.stopObserving()} title={t('cancelObservation')}
            height={40} width={150} buttonStyle={Bs.endButton}
            gradientColorStart={Colors.neutralButton} gradientColorEnd={Colors.neutralButton} shadowColor={Colors.neutralShadow}
            textStyle={Ts.buttonText} iconName={'stop'} iconType={'material-icons'} iconSize={22} contentColor={Colors.darkText}
          />
        </View>
      </View>
    </View>)
}

export default UnfinishedEventViewComponent