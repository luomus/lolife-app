import React, { useState, useEffect, ReactChild } from 'react'
import { View, Text, ScrollView, BackHandler } from 'react-native'
import UserInfoComponent from './UserInfoComponent'
import ObservationEventListComponent from './ObservationEventListElementComponent'
import { useTranslation } from 'react-i18next'
import Cs from '../../styles/ContainerStyles'
import Ts from '../../styles/TextStyles'
import { useDispatch, useSelector } from 'react-redux'
import {
  rootState,
  DispatchType,
  toggleObserving,
  setObservationEventInterrupted,
  setObservationId,
  setCurrentObservationZone,
  setMessageState,
  beginObservationEvent,
  continueObservationEvent,
  logoutUser,
  resetReducer
} from '../../stores'
import { useBackHandler, useClipboard } from '@react-native-community/hooks'
import MessageComponent from '../general/MessageComponent'
import { withNavigation } from 'react-navigation'
import ActivityComponent from '../general/ActivityComponent'
import AppJSON from '../../../app.json'
import storageService from '../../services/storageService'
import { HomeIntroductionComponent } from './HomeIntroductionComponent'
import NewEventWithoutZoneComponent from './NewEventWithoutZoneComponent'
import UnfinishedEventViewComponent from './UnfinishedEventViewComponent'
import NewEventWithZoneComponent from './NewEventWithZoneComponent'

interface BasicObject {
  [key: string]: any
}

type Props = {
  isFocused: () => boolean,
  onLogout: () => void,
  onPressMap: () => void,
  onPressObservationEvent: (id: string) => void,
  onPressFinishObservationEvent: (sourcePage: string) => void,
  obsStopped: boolean,
  navigation: any,
  children?: ReactChild
}

const HomeComponent = (props: Props) => {
  const [pressCounter, setPressCounter] = useState<number>(0)
  const [observationEvents, setObservationEvents] = useState<Element[]>([])
  const [loading, setLoading] = useState<boolean>(false)

  const { observationEvent, observationZone, observing } = useSelector((state: rootState) => {
    return {
      observationEvent: state.observationEvent,
      observationZone: state.observationZone,
      observing: state.observing
    }
  })

  const dispatch: DispatchType = useDispatch()

  const { t } = useTranslation()
  const [data, setString] = useClipboard()
  let logTimeout: NodeJS.Timeout | undefined

  useEffect(() => {
    //set first zone in array as selected zone to avoid undefined values
    if (observationZone.currentZoneId === '' && observationZone.zones.length > 0) {
      dispatch(setCurrentObservationZone(observationZone?.zones[0].id))
    }
  }, [observationZone])

  useEffect(() => {
    const length = observationEvent.events.length
    let isUnfinished: boolean = false

    if (length >= 1) {
      isUnfinished = !observationEvent.events[length - 1].gatheringEvent.dateEnd
    }

    if (isUnfinished) {
      dispatch(toggleObserving())
      dispatch(setCurrentObservationZone(getLastZoneId()))
      dispatch(setObservationEventInterrupted(true))
    }
  }, [])

  useEffect(() => {
    loadObservationEvents()
  }, [observationEvent, observing])

  useEffect(() => {
    if (pressCounter > 0 && !logTimeout) {
      logTimeout = setTimeout(() => {
        setPressCounter(0)
        logTimeout = undefined
      }, 5000)
    }

    const logHandler = async () => {
      if (pressCounter === 5) {
        const logs: any[] = await storageService.fetch('logs')
        clipboardConfirmation(logs)
        setPressCounter(0)
      }
    }

    logHandler()
  }, [pressCounter])

  const loadObservationEvents = () => {
    const events: Array<Element> = []
    const indexLast: number = observationEvent.events.length - 1
    observationEvent.events.forEach((event: BasicObject, index: number) => {
      if (observing && index === indexLast) {
        return
      }

      events.push(<ObservationEventListComponent key={event.id} observationEvent={event} onPress={() => props.onPressObservationEvent(event.id)} />)
    })

    setObservationEvents(events)
  }

  //handle back press in homescreen by asking user if they wish to exit the app
  useBackHandler(() => {
    if (props.isFocused() && observing) {
      dispatch(setMessageState({
        type: 'dangerConf',
        messageContent: t('exit app?'),
        cancelLabel: t('cancel'),
        okLabel: t('exit'),
        onOk: () => BackHandler.exitApp()
      }))

      return true
    }

    return false
  })

  const onBeginObservationEvent = async (zoneUsed: boolean) => {
    const title: string = t('notification title')
    const body: string = t('notification body')
    try {
      await dispatch(beginObservationEvent(props.onPressMap, zoneUsed, title, body))
    } catch (error) {
      dispatch(setMessageState({
        type: 'err',
        messageContent: error.message,
        onOk: () => {
          props.onLogout()
          dispatch(logoutUser())
          dispatch(resetReducer())
        }
      }))
    }
  }

  const onContinueObservationEvent = async () => {
    const title: string = t('notification title')
    const body: string = t('notification body')
    try {
      await dispatch(continueObservationEvent(props.onPressMap, title, body))
    } catch (error) {
      dispatch(setMessageState({
        type: 'err',
        messageContent: error.message,
        onOk: () => {
          props.onLogout()
          dispatch(logoutUser())
          dispatch(resetReducer())
        }
      }))
    }
  }

  const getLastZoneId = () => {
    const zone = observationZone.zones.find((zone: Record<string, any>) => {
      return zone.name === observationEvent.events?.[observationEvent.events.length - 1].gatherings[0].locality
    })

    return zone ? zone.id : ''
  }

  const stopObserving = () => {
    dispatch(setMessageState({
      type: 'dangerConf',
      messageContent: t('stop observing'),
      okLabel: t('cancelObservation'),
      cancelLabel: t('do not stop'),
      onOk: () => {
        dispatch(setObservationId({
          eventId: observationEvent?.events?.[observationEvent?.events?.length - 1].id,
          unitId: null
        }))
        props.onPressFinishObservationEvent('HomeComponent')
      }
    }))
  }

  const showError = (error: string) => {
    dispatch(setMessageState({
      type: 'err',
      messageContent: error
    }))
  }

  const clipboardConfirmation = (logs: any[] | null) => {
    if (logs !== null && logs !== []) {
      dispatch(setMessageState({
        type: 'dangerConf',
        messageContent: t('copy log to clipboard?'),
        okLabel: t('yes'),
        cancelLabel: t('no'),
        onOk: () => setString(JSON.stringify(logs, null, '  '))
      }))
    } else {
      dispatch(setMessageState({
        type: 'msg',
        messageContent: t('no logs')
      }))
    }
  }

  if (loading) {
    return (
      <ActivityComponent text={'loading'} />
    )
  } else {
    return (
      <>
        <ScrollView contentContainerStyle={Cs.outerVersionContainer}>
          <View style={{ justifyContent: 'flex-start' }}>
            <UserInfoComponent onLogout={props.onLogout} />
            <View style={Cs.homeContainer}>
              <HomeIntroductionComponent />
              <View style={{ height: 10 }}></View>
              {observing ? null :
                <NewEventWithoutZoneComponent onBeginObservationEvent={(zoneUsed) => {
                  onBeginObservationEvent(zoneUsed)
                }} />
              }
              <View style={{ height: 10 }}></View>
              <View style={Cs.observationEventContainer}>
                {observing ?
                  <UnfinishedEventViewComponent
                    onContinueObservationEvent={onContinueObservationEvent}
                    stopObserving={stopObserving}
                  />
                  :
                  <NewEventWithZoneComponent
                    setLoading={setLoading}
                    onBeginObservationEvent={onBeginObservationEvent}
                    showError={showError}
                  />
                }
              </View>
              <View style={{ height: 10 }}></View>
              <View style={Cs.observationEventListContainer}>
                <Text style={Ts.previousObservationsTitle}>{t('previous observation events')}</Text>
                {observationEvents}
              </View>
              <View style={{ height: 10 }}></View>
            </View>
          </View>
          <View style={Cs.versionContainer}>
            <Text
              style={Ts.alignedRightText}
              onPress={() => setPressCounter(pressCounter + 1)}>
              {t('version')} {AppJSON.expo.version}
            </Text>
          </View>
        </ScrollView>
        {props.children}
        <MessageComponent />
      </>
    )
  }
}

export default withNavigation(HomeComponent)