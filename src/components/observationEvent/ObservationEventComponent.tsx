import React, { useState, ReactChild } from 'react'
import { View, Text, ScrollView } from 'react-native'
import Cs from '../../styles/ContainerStyles'
import Ts from '../../styles/TextStyles'
import Bs from '../../styles/ButtonStyles'
import Colors from '../../styles/Colors'
import { useDispatch, useSelector } from 'react-redux'
import {
  rootState,
  DispatchType,
  setObservationId,
  uploadObservationEvent,
  setMessageState,
  deleteObservation,
  deleteObservationEvent,
  logoutUser,
  resetReducer
} from '../../stores'
import { useTranslation } from 'react-i18next'
import SendEventModalComponent from '../general/SendEventModalComponent'
import ObservationInfoComponent from './ObservationInfoComponent'
import MessageComponent from '../general/MessageComponent'
import { parseDateForUI } from '../../helpers/dateHelper'
import ActivityComponent from '../general/ActivityComponent'
import ButtonComponent from '../general/ButtonComponent'
import i18n from 'i18next'

type Props = {
  id: string,
  onPressHome: () => void,
  onPressObservation: (sourcePage?: string) => void,
  onPressObservationEvent: (sourcePage?: string) => void,
  onLogout: () => void,
  children?: ReactChild
}

const ObservationEventComponent = (props: Props) => {

  const [sending, setSending] = useState<boolean>(false)
  const [modalVisibility, setModalVisibility] = useState<boolean>(false)

  const credentials = useSelector((state: rootState) => state.credentials)
  const observationEvent = useSelector((state: rootState) => state.observationEvent)

  const dispatch: DispatchType = useDispatch()

  const { t } = useTranslation()

  const event: Record<string, any> | null = observationEvent.events.find(e => e.id === props.id) || null
  const observations: Record<string, any>[] = event?.gatherings[0]?.units || null

  const showMessage = (content: string) => {
    dispatch(setMessageState({
      type: 'msg',
      messageContent: content
    }))
  }

  const showDeleteObservation = (eventId: string, unitId: string) => {
    dispatch(setMessageState({
      type: 'dangerConf',
      messageContent: t('remove observation?'),
      cancelLabel: t('do not delete'),
      okLabel: t('delete'),
      onOk: () => handleDeleteObservation(eventId, unitId)
    }))
  }

  const handleDeleteObservation = async (eventId: string, unitId: string) => {
    try {
      await dispatch(deleteObservation(eventId, unitId))
    } catch (error) {
      dispatch(setMessageState({
        type: 'err',
        messageContent: error.message
      }))
    }
  }

  const showDeleteObservationEvent = (eventId: string) => {
    dispatch(setMessageState({
      type: 'dangerConf',
      messageContent: t('remove observation event?'),
      cancelLabel: t('do not delete'),
      okLabel: t('delete'),
      onOk: () => handleDeleteObservationEvent(eventId)
    }))
  }

  const handleDeleteObservationEvent = async (eventId: string) => {
    try {
      await dispatch(deleteObservationEvent(eventId))
      props.onPressHome()
    } catch (error) {
      dispatch(setMessageState({
        type: 'err',
        messageContent: error.message
      }))
    }
  }

  const sendObservationEvent = async () => {
    setModalVisibility(false)
    setSending(true)
    try {
      await dispatch(uploadObservationEvent(event?.id, credentials, i18n.language))
      showMessage(t('post success'))
      props.onPressHome()
    } catch (error) {
      if (error.message !== t('user token has expired')) {
        dispatch(setMessageState({
          type: 'err',
          messageContent: error.message,
          onOk: () => props.onPressHome()
        }))
      } else {
        dispatch(setMessageState({
          type: 'err',
          messageContent: error.message,
          onOk: () => {
            props.onLogout()
            dispatch(logoutUser())
            dispatch(resetReducer())
          }
        }))
      }
    }

    setSending(false)
  }

  if (!event || !observations) {
    return (
      <ActivityComponent text={t('loading')} />
    )
  } else if (sending) {
    return (
      <ActivityComponent text={t('sending')} />
    )
  } else {
    return (
      <View style={Cs.singleObservationEventContainer}>
        <ScrollView>
          <View style={Cs.eventTopContainer}>
            <View style={Cs.eventTextContainer}>
              <Text>{t('dateBegin')}: {parseDateForUI(event.gatheringEvent.dateBegin)}</Text>
              <Text>{t('dateEnd')}: {parseDateForUI(event.gatheringEvent.dateEnd)}</Text>
              <Text>{t('zone')}: {event.gatherings[0].locality ? event.gatherings[0].locality : t('no zone')}</Text>
            </View>
            <View style={Cs.eventButtonsContainer}>
              <View style={Cs.padding5Container}>
                <ButtonComponent onPressFunction={() => {
                  const id = {
                    eventId: event.id,
                    unitId: ''
                  }
                  dispatch(setObservationId(id))
                  props.onPressObservationEvent('ObservationEventComponent')
                }}
                title={undefined} height={40} width={45} buttonStyle={Bs.editEventButton}
                gradientColorStart={Colors.primaryButton1} gradientColorEnd={Colors.primaryButton2} shadowColor={Colors.primaryShadow}
                textStyle={Ts.buttonText} iconName={'edit'} iconType={'material-icons'} iconSize={22} contentColor={Colors.whiteText}
                />
              </View>
              <View style={Cs.padding5Container}>
                <ButtonComponent onPressFunction={() => setModalVisibility(true)} title={undefined}
                  height={40} width={45} buttonStyle={Bs.sendEventButton}
                  gradientColorStart={Colors.primaryButton1} gradientColorEnd={Colors.primaryButton2} shadowColor={Colors.primaryShadow}
                  textStyle={Ts.buttonText} iconName={'send'} iconType={'material-icons'} iconSize={22} contentColor={Colors.whiteText}
                />
              </View>
              <View style={Cs.padding5Container}>
                <ButtonComponent onPressFunction={() => showDeleteObservationEvent(event.id)} title={undefined}
                  height={40} width={45} buttonStyle={Bs.removeEventButton}
                  gradientColorStart={Colors.neutralButton} gradientColorEnd={Colors.neutralButton} shadowColor={Colors.neutralShadow}
                  textStyle={Ts.buttonText} iconName={'delete'} iconType={'material-icons'} iconSize={22} contentColor={Colors.darkText}
                />
              </View>
            </View>
          </View>
          <Text style={Ts.observationText}>{t('observations')}:</Text>
          {observations.map(observation =>
            <View key={observation.id}>
              <ObservationInfoComponent
                event={event}
                observation={observation}
                editButton={
                  <ButtonComponent onPressFunction={() => {
                    const id = {
                      eventId: event.id,
                      unitId: observation.id
                    }
                    dispatch(setObservationId(id))
                    props.onPressObservation()
                  }}
                  title={t('edit button')} height={40} width={150} buttonStyle={Bs.basicPrimaryButton}
                  gradientColorStart={Colors.primaryButton1} gradientColorEnd={Colors.primaryButton2} shadowColor={Colors.primaryShadow}
                  textStyle={Ts.buttonText} iconName={'edit'} iconType={'material-icons'} iconSize={22} contentColor={Colors.whiteText}
                  />
                }
                removeButton={
                  <ButtonComponent onPressFunction={() => {
                    showDeleteObservation(event.id, observation.id)
                  }}
                  title={t('remove')} height={40} width={150} buttonStyle={Bs.basicNeutralButton}
                  gradientColorStart={Colors.neutralButton} gradientColorEnd={Colors.neutralButton} shadowColor={Colors.neutralShadow}
                  textStyle={Ts.buttonText} iconName={'delete'} iconType={'material-icons'} iconSize={22} contentColor={Colors.darkText}
                  />
                }
              />
              <View style={{ padding: 5 }}></View>
            </View>
          )}
          <SendEventModalComponent modalVisibility={modalVisibility} onCancel={setModalVisibility} sendObservationEvent={sendObservationEvent} />
          {props.children}
          <MessageComponent />
        </ScrollView>
      </View>
    )
  }
}

export default ObservationEventComponent